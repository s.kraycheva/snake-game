# Snake game

Classic snake game with vanilla JS, canvas ,Html and CSS.
The player starts the game with moving in direction. The snake is painted in lime , while the food is red and the board is black. The food is place at random places every time the snake eats it.

The snake cannot go left and right at the same row.The snake body is growing once it eats the food.

The game is over if the player:

- moves outside the board
- snake crosses its body


